<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dev_kaplan');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'auuc56*y^q8Cuw3 $l|Cbb.;O-i2rFnXuZX;S}x(|MKRS.BKjoGb+9A@%<P &/D;');
define('SECURE_AUTH_KEY',  'd+b<+oTux!sduFXOBdkmq)-90>S_vas}5 v*L2A/0679.6+LNu{Y.YW>Al2N0.GQ');
define('LOGGED_IN_KEY',    ']cZ7-`xINyF9ZGJhZZ,{4Ibm7L(kJ&zy_3D[VHpx`<(2I$Rrcz;9HR.5j0Q%^+3v');
define('NONCE_KEY',        'fa3dJm$)>*A.!HXKZ-QL~1BzOZLl+qB+3-$r&HKqd>-dmaZ:Lg_NS[<|+7FZ`G8Z');
define('AUTH_SALT',        'TmQP3.K$HE-9-y*v*Fr^F?fq}.#pVO/@W9A&xdC(x4[]&#=rN+OA-]a}^U);B|6d');
define('SECURE_AUTH_SALT', 'A5h;D`%fX>~hx (qV}b/(dmLSr)i-nG[M4](+]]EUl>KIm;x*s~|!9u;68~l#q2&');
define('LOGGED_IN_SALT',   'U7;(%T(7$|}b-W*kZmloiH>E+u2rj=?l~>D~WiH]g~lS+ma>(qB4iHaS&mjF1I3+');
define('NONCE_SALT',       '_fF+;Z_da6vm+C*P[^|Ki(?qZMe}-mc$Z35XTTBz1]m|9l~#m/7Bk#y+K#h^}L_#');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
