<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

    <div id="inner-footer" class="wrap cf">

        <nav role="navigation">
            <?php
            wp_nav_menu(array(
                'container' => 'div', // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
                'container_class' => 'footer-links cf', // class of container (should you choose to use it)
                'menu' => __('Footer Links', 'bonestheme'), // nav name
                'menu_class' => 'nav footer-nav cf', // adding custom nav class
                'theme_location' => 'footer-links', // where it's located in the theme
                'before' => '', // before the menu
                'after' => '', // after the menu
                'link_before' => '', // before each link
                'link_after' => '', // after each link
                'depth' => 0, // limit the depth of the nav
                'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
            ));
            ?>
        </nav>

        <p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.</p>

    </div>

</footer>

</div>
<!-- Bootstrap core JS -->
<script src="<?php echo get_template_directory_uri(); ?>/js/lib/jquery-1.11.2.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/bootstrap-3.3.4/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/kaplan.js"></script>
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/lib/responsive_ie_tool/respond.min.js"></script>
<![endif]-->

<?php // all js scripts are loaded in library/bones.php  ?>
<?php wp_footer(); ?>

</body>

</html> <!-- end of site. what a ride! -->
