<?php
/*
  Template Name: Custom Page Three Column
 *
 */
?>

<?php get_header(); ?>

<div id="content">

    <div id="inner-content" class="wrap cf">

        <main id="main" class="col-sm-12 col-md-12 col-lg-12" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

                        <header class="article-header">

                            <h1 class="page-title"><?php the_title(); ?></h1>

                            <p class="byline vcard">
                                <?php printf(__('Posted <time class="updated" datetime="%1$s" itemprop="datePublished">%2$s</time> by <span class="author">%3$s</span>', 'bonestheme'), get_the_time('Y-m-j'), get_the_time(get_option('date_format')), get_the_author_link(get_the_author_meta('ID'))); ?>
                            </p>
                            
                        </header>

                        <section class="entry-content cf content-feed-template" itemprop="articleBody">
                            <?php the_content(); ?>
                            
                            <div class="col-sm-4 col-feed-list">
                                <h2><?php _e("Top stories","kaplan");?></h2>
                                <?php 
                                    get_feed($rss_top_stories);
                                ?>
                            </div>
                            <div class="col-sm-4 col-feed-list">
                                <h2><?php _e("Business","kaplan");?></h2>
                                <?php 
                                    get_feed($rss_business);
                                ?>
                            </div>
                            <div class="col-sm-4 col-feed-list">
                                <h2><?php _e("Technology","kaplan");?></h2>
                                <?php 
                                    get_feed($rss_technology);
                                ?>
                            </div>
                            
                        </section>
                        
                        

                    </article>

                <?php endwhile;
            else : ?>

                <article id="post-not-found" class="hentry cf">
                    <header class="article-header">
                        <h1><?php _e('Oops, Post Not Found!', 'bonestheme'); ?></h1>
                    </header>
                    <section class="entry-content">
                        <p><?php _e('Uh Oh. Something is missing. Try double checking things.', 'bonestheme'); ?></p>
                    </section>
                    <footer class="article-footer">
                        <p><?php _e('This is the error message in the page-custom.php template.', 'bonestheme'); ?></p>
                    </footer>
                </article>

<?php endif; ?>

        </main>

    </div>

</div>


<?php get_footer(); ?>
